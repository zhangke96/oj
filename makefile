ojClient:main.cpp judge_client
	g++-5 -std=c++11 main.cpp -o ojClient `mysql_config --cflags --libs`
judge_client:judge_client.cpp
	g++-5 judge_client.cpp -o judge_client `mysql_config --cflags --libs`
judge:judge.c
	gcc judge.c -o judge `mysql_config --cflags --libs`
judgecpp:judge.cpp
	g++-5 judge.cpp -std=c++11 -o judgecpp `mysql_config --cflags --libs`
